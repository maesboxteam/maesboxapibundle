<?php

namespace Maesbox\ApiBundle\Model\Traits;

use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;

trait BaseRestControllerTrait
{
	/**
	 * @var View 
	 */
	protected $view;
	
	public function __construct()
	{
		$this->setView($this->createView());
	}
	
	/**
	 * @param array $groups
	 */
	protected function setViewGroups(array $groups = [])
	{
		$reflection = new \ReflectionClass($this->getView());
		
		if($reflection->hasMethod("setSerializationContext") && $reflection->hasMethod("getSerializationContext")){
			$this->getView()->setSerializationContext($this->getView()->getSerializationContext()->setGroups($groups));
		}
		elseif($reflection->hasMethod("setContext") && $reflection->hasMethod("getContext"))
		{
			$this->getView()->setContext($this->getView()->getContext()->setGroups($groups));
		}
	}
	
	/**
	 * @param mixed $data
	 */
	protected function setViewData($data)
	{
		$this->getView()->setData($data);
	}
	
	protected function setViewCode($code)
	{
		$this->getView()->setStatusCode($code);
	}
	
    /**
     * @return View
     */
    protected function createView()
    {
        return View::create();
    }

    /**
     * @param View $view
     */
    public function handleView(View $view = null)
    {
		$view = (!$view)? $this->getView() : $view;
		
		$reflection = new \ReflectionClass($view);
		
		if($reflection->hasMethod("setSerializationContext") && $reflection->hasMethod("getSerializationContext")){
			$view->setSerializationContext($view->getSerializationContext()->enableMaxDepth());
		}
		elseif($reflection->hasMethod("setContext") && $reflection->hasMethod("getContext"))
		{
			$view->setContext($view->getContext()->enableMaxDepth());
		}
        
        return parent::handleView($view);
    }
	
	/**
	 * @param View $view
	 * @return $this
	 */
	protected function setView(View $view)
	{
		$this->view = $view;
		return $this;
	}
	
	/**
	 * @return View
	 */
	protected function getView()
	{
		return $this->view;
	}
	
	/**
	 * @param ParamFetcherInterface $param_fetcher
	 * @param array $params
	 * @return array
	 */
	protected function processFilterParameter(ParamFetcherInterface $param_fetcher, array $params = [])
	{
		$filter_by = [];
		
		foreach($params as $to => $from)
		{
			$param = $param_fetcher->get($from);
			
			if($param !== null){
				$filter_by[$to] = $param;
			}
		}
		
		return $filter_by;
	}
	
	/**
     * @param PaginationInterface $data
     * @param View|null           $view
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handleListView(PaginationInterface $data)
    {
        $this->setViewData([
            "data" => $data->getItems(),
            "pagination" => [
                "total_item_count"  => $data->getTotalItemCount(),
                "page_item_count"   => $data->getItemNumberPerPage(),
                "page"              => $data->getCurrentPageNumber()
            ]
        ]);

        return $this->handleView();
    }
}
