<?php

namespace Maesbox\ApiBundle\Model\BaseClass;

use FOS\RestBundle\Controller\FOSRestController;

use Maesbox\CommonBundle\Model\Traits\BaseControllerTrait;
use Maesbox\ApiBundle\Model\Traits\BaseRestControllerTrait;
	
abstract class BaseRestController extends FOSRestController
{
    use BaseControllerTrait;
	use BaseRestControllerTrait;
}
