MaesboxApiBundle
===

[![Latest Stable Version](https://poser.pugx.org/maesbox/api-bundle/v/stable)](https://packagist.org/packages/maesbox/api-bundle)
[![Total Downloads](https://poser.pugx.org/maesbox/api-bundle/downloads)](https://packagist.org/packages/maesbox/api-bundle)
[![License](https://poser.pugx.org/maesbox/api-bundle/license)](https://packagist.org/packages/maesbox/api-bundle)
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/dd9e23b3-78d9-490f-a04d-7eff31acc728/mini.png)](https://insight.sensiolabs.com/projects/dd9e23b3-78d9-490f-a04d-7eff31acc728)

this bundle provides base functionnalities for rest api's

Installation
---

### composer 

```
composer require maesbox/apibundle ~1.1
```

### kernel

add these lines to `app/AppKernel.php` 

```php
<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
		...
			new FOS\RestBundle\FOSRestBundle(),
			new Nelmio\CorsBundle\NelmioCorsBundle(),
			new Nelmio\ApiDocBundle\NelmioApiDocBundle(),
			new Maesbox\CommonBundle\MaesboxCommonBundle(),
			new Maesbox\ApiBundle\MaesboxApiBundle(),

		...
		);
	}
	...
}
```

### routing

add these lines to `app/routing.yml`

```yaml
MaesboxApiBundle:
	resource: "@MaesboxApiBundle/Resources/config/routing.yml"
	prefix:   /api
```

**note:** le routing du nelmio/api-doc-bundle est ajouté par défaut

Features
---

### controller

the bundle provides base classe for rest controller 
please refer to [friendofsymfony/rest-bundle][1] [documentation][2] for more informations

```php
<?php

use Maesbox\ApiBundle\BaseClass\BaseRestController;

class AppController extends BaseRestController
{

}

```

### exception listener

the bundle provides an exception listener in order to convert exception to json response

add this lines to config.yml

```yml
maesbox_api:
	exception_listener: true
```


[1]: https://github.com/FriendsOfSymfony/FOSRestBundle
[2]: http://symfony.com/doc/master/bundles/FOSRestBundle/index.html


